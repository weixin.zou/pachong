#!/usr/bin/env python
# -*- coding:utf-8 -*-
# 作者：zouweixin
# 网址：https://gitlab.com/weixin.zou
import xlwings as xw
#创建新的Excel文件
#方法一：创建一个新app，在app下新建一个book
# wb = xw.Book()
# wb.save('1.xlsx')
# wb.close
#方法二：当前app下新建book，visible参数控制创建文件时可见的属性
app=xw.App(visible=False,add_book=False)
wb=app.books.add()
wb.save('2.xlsx')
wb.close()
app.quit() #结束进程
#实例：打开xlsx文件
# app = xw.App(visible=True,add_book=False)
# app.display_alerts=False #不显示xlsx的消息框
# app.screen_updating=False #关闭屏幕更新，加快宏的执行速度
# wb = app.books.open('2.xlsx')
# print(wb.fullname)
# wb.save()
# wb.close()
# app.quit()
# #以第一种方式创建Book时，打开文件的操作
# wb = xw.Book('2.xlsx')
#https://blog.csdn.net/th1522856954/article/details/107850947




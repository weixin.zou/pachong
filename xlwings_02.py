#!/usr/bin/env python
# -*- coding:utf-8 -*-
# 作者：zouweixin
# 网址：https://gitlab.com/weixin.zou

import xlwings as xw
#使用books方法，引用功能清单工作簿
app=xw.App(visible=True,add_book=False)
app.display_alerts=False   #不显示Excel消息框
app.screen_updating=False  #关闭屏幕更新,可加快宏的执行速度
wb=app.books.open('功能清单.xls')
# print(wb.fullname)       # 输出打开的excle的绝对路径
wb.save()
wb.close()
app.quit()  # 退出excel程序，
# app.kill() 通过杀掉进程强制Excel app退出

# 以第一种方式创建Book时，打开文件的操作可如下
wb = xw.Book('功能清单.xls')
# 在A1单元格写入值
# 实例化一个工作表对象
sheet1 = wb.sheets["sheet1"]
# 或者
# sheet1 =xw.books['1.xlsx'].sheets['sheet1']
# print(sheet1.name) 输出工作簿名称
# 写入值
sheet1.range('A1').value = 'python功能清单01'
# 读值并打印
print('value of A1:', sheet1.rangec('A1').value)
# 清空单元格内容,如果A1中是图片，此方法没有效果
sheet1.range('A1').clear()

# 传入列表写入多行值
sheet1 = 'bu shi ba a sir '
sheet1.range('A1').value = [['a', 'b', 'c'],[1,2,3]]
# 当然也可以将pandas的DataFrame数据写入
import pandas as pd
df = pd.DataFrame([[1, 2], [3, 4]], columns=['A', 'B'])
sheet1.range('A1').value = df
# 读取数据，输出类型为DataFrame
sheet1.range('A1').options(pd.DataFrame, expand='table').value
# 支持添加图片的操作
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
plt.plot(100, np.log(100))
sheet1.pictures.add(fig, name='MyPlot', update=True)









